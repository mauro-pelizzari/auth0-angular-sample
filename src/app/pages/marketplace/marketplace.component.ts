import { Component, OnInit } from '@angular/core';

import { AuthService } from '@auth0/auth0-angular';

@Component({
  selector: 'app-marketplace',
  templateUrl: './marketplace.component.html'
})
export class MarketplaceComponent implements OnInit {

  profileJson: string = null;

  constructor(public auth: AuthService) {}

  ngOnInit(): void {
    console.log("user auth = " + this.auth.isAuthenticated$);
    this.auth.user$.subscribe(
      (profile) => (this.profileJson = JSON.stringify(profile, null, 2))
    );
  }

}
