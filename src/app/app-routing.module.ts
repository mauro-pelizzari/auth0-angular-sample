import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from 'src/app/pages/home/home.component';
import { ExternalApiComponent } from 'src/app/pages/external-api/external-api.component';
import { MarketplaceComponent } from 'src/app/pages/marketplace/marketplace.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full',
  },
  {
    path: 'marketplace',
    component: MarketplaceComponent,
  },
  {
    path: 'external-api',
    component: ExternalApiComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
